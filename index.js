// Short Version - Complete
let ash = {
	name: "Ash Ketchum", age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: { hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty'] },
	talk: function(pokemon) {
		console.log(pokemon.name + "! I choose you!");
		return pokemon;
	}
}
function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health -= this.attack));
		if (target.health < 1) {
			console.log(target.name + " fainted");
		}
		console.log(target);
	}
}
let pikachu = new Pokemon ("Pikachu", 12);
let geodude = new Pokemon ("Geodude", 8);
let mewtwo = new Pokemon ("Mewtwo", 100);
console.log(ash);
console.log("Result of dot notation:");
console.log(ash.name);
console.log("Result of square bracket notation:");
console.log(ash['pokemon']);
console.log(ash.talk(pikachu));
console.log(geodude);
console.log(mewtwo);
geodude.tackle(pikachu);
mewtwo.tackle(geodude);


// Long Version (Reusable) - Complete
/*
function Trainer(name, age, pokemon, friends) {
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;
	this.friends = friends;
	this.talk = function(pokemon) {
		console.log(pokemon.name + "! I choose you!");
		return pokemon;
	};
}

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health -= this.attack));
		if (target.health < 1) {
			console.log(target.name + " fainted");
		}
		console.log(target);
	}
}

let pikachu = new Pokemon ("Pikachu", 12);
let geodude = new Pokemon ("Geodude", 8);
let mewtwo = new Pokemon ("Mewtwo", 100);
let ash = new Trainer (
	"Ash Ketchum",	10,
	["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	{ hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty'] },
	function(pokemon) {
		console.log(pokemon + "! I choose you!");
		return pokemon;
	}
)
console.log(ash);
console.log("Result of dot notation:");
console.log(ash.name);
console.log("Result of square bracket notation:");
console.log(ash['pokemon']);
console.log(ash.talk(pikachu));
console.log(geodude);
console.log(mewtwo);
geodude.tackle(pikachu);
mewtwo.tackle(geodude);
*/